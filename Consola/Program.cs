﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/// <summary>
/// Este es el namespace general
/// </summary>
namespace Consola
{
    /// <summary>
    /// Este es la clase que me va a ejecutar la consola
    /// </summary>
    class Program
    {
        /// <summary>
        /// Este es el metodo principal
        /// </summary>
        /// <param name="args">Contiene los argumentos para ejecutar el metodo</param>
        static void Main(string[] args)
        {
            // esto es un comentario
            //TODO: Escribir condigo pendiente
            #region
            Console.Write("Hola");
            Console.WriteLine(" Mi nombre Es");
            Console.Write(ConfigurationManager.AppSettings["Nombre"].ToString());
            #endregion
            int X = 5;
            {
                if (X >= (int)EnumEjemplo.a)
                {
                    Console.WriteLine("Es mayor o igual");
                }

                else if (X == 0)
                {
                    Console.WriteLine("Es cero");
                }
                else
                {
                    Console.WriteLine("es menor");
                }



            }
            int b = (int)EnumEjemplo.a;
            double C = Math.PI;


            Console.WriteLine(C);
            Console.Write(b);
            Console.Read();



            /*Este es el comentario final
             y está en dos linea
             */
            
        }

        /// <summary>
        /// Enumeracion de Ejemplo
        /// </summary>
        public enum EnumEjemplo
        {
            /// <summary>
            /// Primer Valor
            /// </summary>
            a = 10,
            /// <summary>
            /// Segundo Valor
            /// </summary>
            b = 20,
        }
    }

}
